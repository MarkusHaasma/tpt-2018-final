var config = require('../../nightwatch.conf.js');

module.exports = {
    'example google': function(browser) {
        browser
            .url('https://www.google.ee/')
            .resizeWindow(1200,800)
            .waitForElementVisible('body div#main')
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .assert.containsText('body','tallinn')
            .saveScreenshot(config.imgpath(browser) + 'GoogleTest.png')
            .pause(2000)
            .click('h3[class="LC20lb"]')
            .saveScreenshot(config.imgpath(browser) + 'TallinnTest.png')
            .pause(2000)
            .end();
    }
};
