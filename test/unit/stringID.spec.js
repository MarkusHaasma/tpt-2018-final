const users = require('../../src/user');

test('Exception', () => {
    expect(() => {
        users("Suvakas")
    }).toThrowError("id needs to be integer");
});
